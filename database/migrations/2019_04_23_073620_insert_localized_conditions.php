<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertLocalizedConditions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('localized_conditions')->insert([
            'name' => 'VIVA (SIN SER VÍCTIMA DE DELITO)'
        ]);
        DB::table('localized_conditions')->insert([
            'name' => 'VIVA (VÍCTIMA DE DELITO)'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
