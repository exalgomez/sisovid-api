<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataCrimes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('crimes')->insert([
            'name' => 'AUSENTE'
        ]);
        DB::table('crimes')->insert([
            'name' => 'EXTRAVÍO'
        ]);
        DB::table('crimes')->insert([
            'name' => 'FEMINICIDIO'
        ]);
        DB::table('crimes')->insert([
            'name' => 'FORZADA'
        ]);
        DB::table('crimes')->insert([
            'name' => 'HOMICIDIO'
        ]);
        DB::table('crimes')->insert([
            'name' => 'LESIONES'
        ]);
        DB::table('crimes')->insert([
            'name' => 'MALTRATO INFANTIL'
        ]);
        DB::table('crimes')->insert([
            'name' => 'PARRICIDIO'
        ]);
        DB::table('crimes')->insert([
            'name' => 'POR PARTICULARES'
        ]);
        DB::table('crimes')->insert([
            'name' => 'PRIVACION ILEGAL DE LA LIBERTAD'
        ]);
        DB::table('crimes')->insert([
            'name' => 'ROBO'
        ]);
        DB::table('crimes')->insert([
            'name' => 'SUBSTRACCION O ROBO Y TRAFICO DE MENORES'
        ]);
        DB::table('crimes')->insert([
            'name' => 'TORTURA'
        ]);
        DB::table('crimes')->insert([
            'name' => 'TENTATIVA DE HOMICIDIO'
        ]);
        DB::table('crimes')->insert([
            'name' => 'SECUESTRO'
        ]);
        DB::table('crimes')->insert([
            'name' => 'HECHOS DE SANGRE'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
