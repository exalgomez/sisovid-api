<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmberAlertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amber_alert', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('file_id');
            $table->foreign('file_id')->references('id')->on('files');

            $table->unsignedInteger('amber_report_id')->nullable();
            $table->foreign('amber_report_id')->references('id')->on('amber_report');
            $table->string('amber_alert_report_number',200)->nullable();
            $table->date('report_issue_date')->nullable();

            $table->tinyInteger('active')->default(1);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amber_alert');
    }
}
