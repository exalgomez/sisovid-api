<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCirDisappearanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          DB::table('cir_disappearance')->insert([
            'name' => 'SE VIO EN SU DOMICILIO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE TRANSLADABA A SU DOMICILIO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'IBA RUMBO A SU TRABAJO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE LE VIO EN UNA AGENCIA DEL MINISTERIO PÚBLICO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE LE PRIVÓ DE LA LIBERTAD POR PERSONAS ARMADAS EN LA CALLE'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE LE PRIVÓ DE LA LIBERTAD POR PERSONAS ARMADAS EN SU DOMICILIO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE LE PRIVÓ DE LA LIBERTAD POR UNA CORPORACIÓN POLICIACA O MILITAR'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'ÚLTIMO CONTACTO VÍA TELEFÓNICA O MENSAJE DE TEXTO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'REFIRIÓ PROBLEMAS FAMILIARES ANTES DE DESAPARECER'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE IGNORAN LAS CIRCUNSTANCIAS'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SALIÓ DE CASA A REALIZAR ALGUNA ACTIVIDAD Y YA NO REGRESÓ'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'VÍCTIMA DE SECUESTRO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'VIAJABA RUMBO A ESTADOS UNIDOS'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE ENCONTRABA EN SU TRABAJO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE ENCONTRABA EN EL TRAYECTO DE UNA CIUDAD O COMUNIDAD A OTRA'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE LE VIÓ EN EL DOMICILIO DE UN FAMILIAR O CONOCIDO'
          ]);
          DB::table('cir_disappearance')->insert([
            'name' => 'SE DESCONOCE'
          ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
