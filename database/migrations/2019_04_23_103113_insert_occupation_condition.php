<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertOccupationCondition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::table('occupation_condition')->insert([
            'name' => 'ADICCIONES'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'ADOLESCENTE (12 a 17)'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'ADULTO(A) MAYOR'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'DISCAPACITADO(A)'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'EMBARAZO'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'EXTRANJERO(A)'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'IDENTIDAD DE GENERO'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'MARGINACION'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'MIGRANTE'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'NIÑAS Y NIÑOS (0 a 11)'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'PREFERENCIA SEXUAL'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'PUEBLO ORIGINARIO'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'REFUGIADO(A)'
        ]);
        
        DB::table('occupation_condition')->insert([
            'name' => 'RELIGION'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'SERVIDORES PUBLICOS DEDICADOS A IMPARTICION DE JUSTICIA'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'SITUACION DE CALLE'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'SE IGNORA'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'S/D'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'N/A'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'NINGUNA'
        ]);
        DB::table('occupation_condition')->insert([
            'name' => 'NO SE PROPORCIONA'
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
