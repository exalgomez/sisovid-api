<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertJustiStatusFilePreviousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('justi_status_file_previous', function (Blueprint $table) {
            $table->unsignedInteger('type_status_file')->nullable();
        });
        DB::table('justi_status_file_previous')->insert(['name' => 'FALTA UNA O MAS PERSONAS DE SER SER LOCALIZADAS (ACTIVA)','type_status_file' => 1]);
        DB::table('justi_status_file_previous')->insert(['name' => 'HAY VEHICULOS, BIENES, U OTROS INDICIOS ASEGURADOS QUE ESTAN SIENDO INVESTIGADOS (ACTIVA)','type_status_file' => 1]);
        DB::table('justi_status_file_previous')->insert(['name' => 'SE TIENEN PERSONAS DETENIDAS RELACIONADAS A LA DESAPARICION (ACTIVA)','type_status_file' => 1]);
        DB::table('justi_status_file_previous')->insert(['name' => 'ARTICULO 253, NO SE CONSTITUYE DELITO Y SE ABSTIENE DE INVESTIGACION LA FEPD, (CIERRE)','type_status_file' => 2]);
        DB::table('justi_status_file_previous')->insert(['name' => 'SE ACREDITA OTRO DELITO AJENO A LA DESAPARICION (CIERRE)','type_status_file' => 2]);
        DB::table('justi_status_file_previous')->insert(['name' => 'SE ACUMULA INDAGATORIA (CIERRE)','type_status_file' => 2]);
        DB::table('justi_status_file_previous')->insert(['name' => 'SE ACREDITA LA INCOMPETENCIA TERRITORIAL (CIERRE)','type_status_file' => 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
