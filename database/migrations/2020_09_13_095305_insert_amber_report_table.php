<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAmberReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('amber_report')->insert([
            'name' => 'SI'
        ]);
        DB::table('amber_report')->insert([
            'name' => 'NO'
        ]);
        DB::table('amber_report')->insert([
            'name' => 'N/A'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
