<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertOccupationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('occupation')->insert([
            'name' => '1° AGRICULTURA'
        ]);
        DB::table('occupation')->insert([
            'name' => '1° GANADERIA'
        ]);
        DB::table('occupation')->insert([
            'name' => '1° MINERIA'
        ]);
        DB::table('occupation')->insert([
            'name' => '2° MANOFACTURA ARTESANAL'
        ]);
        DB::table('occupation')->insert([
            'name' => '2° MANOFACTURA INDUSTRIAL'
        ]);
        DB::table('occupation')->insert([
            'name' => '3° SERVICIOS SECTOR PRIVADO'
        ]);
        DB::table('occupation')->insert([
            'name' => '3° SERVICIOS SECTOR PUBLICO'
        ]);
        DB::table('occupation')->insert([
            'name' => 'COMERCIANTE (FORMAL)'
        ]);
        DB::table('occupation')->insert([
            'name' => 'COMERCIANTE (INFORMAL)'
        ]);
        DB::table('occupation')->insert([
            'name' => 'DEFENSORES DE DERECHOS HUMANOS'
        ]);
        DB::table('occupation')->insert([
            'name' => 'DESEMPLEADO'
        ]);
        DB::table('occupation')->insert([
            'name' => 'EMPLEADO EN SERVICIOS DE COMUNICACION (PERIODISTA...)'
        ]);
        DB::table('occupation')->insert([
            'name' => 'EMPRESARIO (DUEÑO DE EMPRESA)'
        ]);
        DB::table('occupation')->insert([
            'name' => 'MIEMBRO ACTIVO DE UNA ONG'
        ]);
        DB::table('occupation')->insert([
            'name' => 'PRESTADOR DE SERVICIOS PROFESIONALES (PROFESIONISTAS)'
        ]);
        DB::table('occupation')->insert([
            'name' => 'TRABAJADOR EN SERVICIOS DE LA COSTRUCCIÓN'
        ]);
        DB::table('occupation')->insert([
            'name' => 'TRABAJADOR EN SERVICIOS DE PROTECCIÓN , VIGILANCIA O FUERZAS ARMADAS'
        ]);
        DB::table('occupation')->insert([
            'name' => 'TRABAJADOR EN SERVICIOS DOMÉSTICOS Y DEL HOGAR'
        ]);
        DB::table('occupation')->insert([
            'name' => 'SE IGNORA'
        ]);
        DB::table('occupation')->insert([
            'name' => 'S/D'
        ]);
        DB::table('occupation')->insert([
            'name' => 'N/A'
        ]);
        DB::table('occupation')->insert([
            'name' => 'EMPLEADO'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
