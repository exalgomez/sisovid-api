<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAddictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('addictions')->insert([
            'name' => 'SI'
        ]);
        DB::table('addictions')->insert([
            'name' => 'NO'
        ]);
        DB::table('addictions')->insert([
            'name' => 'LO DESCONOCE LA DENUNCIANTE'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
