<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLocalizedConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('localized_conditions', function (Blueprint $table) {
            $table->unsignedInteger('type_status_file')->nullable();
        });

        DB::table('localized_conditions')
            ->where('name', '=', 'VIVA (SIN SER VÍCTIMA DE DELITO)')
            ->update(['type_status_file' => 3]);

        DB::table('localized_conditions')
            ->where('name', '=', 'VIVA (VÍCTIMA DE DELITO)')
            ->update(['type_status_file' => 3]);
        
        DB::table('localized_conditions')->insert(['name' => 'MUERTA (SIN SER VÍCTIMA DE DELITO)','type_status_file' => 3]);
        DB::table('localized_conditions')->insert(['name' => 'MUERTA (VÍCTIMA DE DELITO)','type_status_file' => 3]);
        DB::table('localized_conditions')->insert(['name' => 'MUERTA (COINCIDENCIA DE ADN SEMEFO)','type_status_file' => 3]);

        DB::table('localized_conditions')->insert(['name' => 'INCOMPETENCIA TERRITORIAL (REMISIÓN A OTRA ENTIDAD)','type_status_file' => 4]);
        DB::table('localized_conditions')->insert(['name' => 'INCOMPETENCIA DEL DELITO (REMISIÓN A SECUESTROS U OTRO)','type_status_file' => 4]);

        DB::table('localized_conditions')->insert(['name' => 'ACUMULADO','type_status_file' => 2]);
        DB::table('localized_conditions')->insert(['name' => 'DUPLICADO','type_status_file' => 2]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
