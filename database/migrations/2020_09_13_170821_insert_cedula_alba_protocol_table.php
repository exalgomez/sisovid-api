<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCedulaAlbaProtocolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('cedula_alba_protocol')->insert([
            'name' => 'SI'
        ]);
        DB::table('cedula_alba_protocol')->insert([
            'name' => 'NO'
        ]);
        DB::table('cedula_alba_protocol')->insert([
            'name' => 'N/A'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
